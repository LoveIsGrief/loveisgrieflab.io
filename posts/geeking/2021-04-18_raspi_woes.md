See that unimpressed man's face? Yeah, that was me starting out (again) with the raspberrypi this week.
<!-- TEASER_END -->

This is the most frustrating device I've had the displeasure of working with to date.
I'm no hardware dude, don't know what a GPIO is, can't weld or solder to save my life, and am generally
 disinterested in hardware beyond it just working.
One could call me a normie when it comes to such things.

Now, software, on the other hand, is fun! At least more often than not.
And, a budding devops person, setting up stuff, getting it running, learning more about a part of the OS...FUN!

The raspi though - **not** fun.

# History

I've owned one other raspberrypi besides the RPi3 and it quit working for some reason.
Dumb and lazy as past me was, when then desire to have a home backup server arose, 
 an upgrade to the RPi3 was chosen - a decision I would learn to regret.

## Bad WiFi (RPi1)

WiFi has been an issue with raspis since the beginning, and it hasn't been [fixed in newer models].
My raspi3 was befallen by choppy, intermittent connections and just [straight up freezes].
It isn't clear to me what's going on, if the firmware is opensource, where the issue actually is,
 but I haven't had a stable connection on it, ever.

LAN just works ™ (seriously, though, never had a problem with it), and I wouldn't use anything else long-term.

## Dead HDD

Not only should a backup server have a stable connection, it should also **not** crash very often.
Unfortunately, and this is my own damn fault, if you plug an appliance onto a raspi 
 that pulls too much power... it shuts off.
When electronics shut off frequently enough without saving state, it will occur at least once when
 that state should've been saved.
Consequences will never be the same!

[![consequences](/images/memes/consequences.jpeg)][consequences]

## Dead SD-card

The default distro for the raspberry pi was called raspian at the time.
They changed the name now and I can't seem to remember it, so it will be raspbian immortalized.

Anyway, that debian offshoot isn't made for SD-cards.
It's as chatty as your next-door grandma that doesn't get visits from her family often.
She has to work off the pent-up social energy and chew your ear off should you be unlucky enough
 to bump into her in the hall.

I'm not going to make assumptions what's loaded into a filesystem on RAM, but clearly, not enough is.
There exists a script to setup the distro to do ([read-only raspbian]), but it should be the default.

# Leading to the present

A flatmate owns a [Gramofon] (another product name that's super easy to search online), which is an expensive
 box to remotely control and play music from leasing services like Spotify.
An unsuspecting me, tried to set it up to play music from my phone, but it never played a single sound,
 because it couldn't get setup.

It should've been a simple task of "hit the big button for a few seconds and connect to bluetooth",
 but this device is WiFi operated, requires a special app, and a subscription to the aforementioned leasing service.
Big bluetooth button players exist, and the idea was to build one of those 
 with the RPi3 that was collecting dust. 

... and so the woes began

## Hunky raspbian

Armed with a 2GB SD, I set out to get the raspberrypi working again.
"Raspian Lite" is 1.6GB!

The sweatdrops were starting to form at this point, but we go onwards!

A guide was found to get bluetooth and pulseaudio working together.
It looked simple enough. `sudo apt-get install pulseaudio`

> ~80MB of packages will be downloaded. ~740MB of disk space will be required

Eyes blinking, mouth dry, arms spaghetti... **740MB**??? To get audio working? 😐
You've... gotta be kidding, right?

Already there, a towel had been prepped to be to chucked across the room.
But luckily, having used docker a lot, there was a distro that had gained popularity
 due to it's miniscule size:

**alpine to the rescue!**

Compared to, well, anything nowadays, alpine is a lightweight. 
It can weigh 5MB which is less than most photos taken.
It was also surreptitious to discover, that it was additionally fast because 
 of loading the entire root filesystem in RAM at boot.

The FS is read-only and RAM is `rw`.

And there's a [guide to setup the RPi3][alpine rpi3]!

Praised be the Swiss, the new masters of efficiency (Germany lost that title long ago).

Alas...

## Not all is gravy in the alps

TODO

--------------

Image by [knollbaco on openclipart.org](https://openclipart.org/detail/190631/face-of-an-angry-man)


[alpine rpi3]: https://wiki.alpinelinux.org/wiki/Raspberry_Pi
[consequences]: https://knowyourmeme.com/memes/events/jessi-slaughter-cyberbulling-controversy
[fixed in newer models]: https://github.com/raspberrypi/linux/issues/3849
[Gramofon]: https://gramofon.com/
[read-only raspbian]: https://www.raspberrypi.org/blog/adafruits-read-only/
[straight up freezes]: https://github.com/raspberrypi/linux/issues/2453
