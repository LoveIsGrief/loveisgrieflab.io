Just how difficult is it to test plymouth bootscreens?
<!-- TEASER_END -->

Helluva annoying...

It took me a whole day to figure out which components were acting together to present a splashscreen 
 at boot time; and I still haven't figured out how to make the boot process smooth.

# Smooth booting

What do I mean with a smooth boot-process?
Well, what I would like is to hit the power button of my machine:

 - immediately get the splashscreen
 - seamlessly transition to a login-screen or a login animation 
 - transition to the desktop.

At the moment, what I get is:

 - a glimpse of "GRUB Menu 2"
 - some kernel messages
 - a black screen for a few milliseconds
 - the splash-screen
 - a black screen for a few milliseconds (again)
 - the login screen

# Actually testing boot splash screens

To test on OpenseSuse 15.2, these are the steps I had to take in order for [plymouth] to simulate a boot.

```bash
#!/bin/bash

# Install Xephyr and X11 renderer for plymout
sudo zypper install plymouth-x11-renderer xorg-x11-server-extra

# start Xephr
Xehpyr :2 &

#####################
# as root (sudo -s)

# point to Xephyr display
export DISPLAY=:2

# get active theme
plymouth-set-default-theme

# list available themes
plymouth-set-default-theme --list

# Set theme
plymouth-set-default-theme breeze # breeze is a theme you need to install

# Test plymouth theme in Xephyr for 10 seconds
plymouthd ; plymouth --show-splash && for ((I=0; I<10; I++)); do sleep 1 ;  plymouth --update=test$I ; done ; plymouth --quit

# Actually apply a theme
plymouth-set-default-theme -R breeze # Notice the -R !
```

Below, a video of me running those commands.

|              |
|:------------:|
| <iframe height="480" width="720" sandbox="allow-same-origin allow-scripts allow-popups" src="https://video.ploud.fr/videos/embed/54bc7003-0bd1-449e-917c-669d27bfca55" frameborder="0" allowfullscreen></iframe> |

To find other bootscreens, installing the KDE store system settings plugin was useful.
The plugin however can't activate the screens on OpenSuse :/
Of course they can also be downloaded from the [KDE store directly][KDE store].

```bash
# From KDE Frameworks
sudo zypper install plymouth-kcm

# Find and download plymouth theme in KDE system settings > Start & Stop
```

# Common problems

A few bootscreens had a few problems and had install scripts specific for debian derivatives.


## Finding the plymouth data directory

On OpenSuse, it's `/usr/share/plymouth`.
Other distributions can find the data directory by tacking on "plymouth" 
 to the datadir found in `plymouth-set-default-theme`.

`grep "PLYMOUTH_DATADIR=" $(sudo which plymouth-set-default-theme)`

## Install script doesn't work on my system

Luckily, installing isn't too difficult and can be done manually.
Say you want to install [Cyanide Plymouth Theme].

After downloading the zip, unzip it to a directory of your choice e.g `/tmp/Cyanide-Plymouth-Theme`.

Then find the `.plymouth` file. For this theme it looks like

```ini
[Plymouth Theme]
Name=cyanide
Description=display a sci-fi ring
Comment=created By Aditya Shakya (@adi1090x)
ModuleName=script

[script]
ImageDir=/usr/share/plymouth/themes/cyanide
ScriptFile=/usr/share/plymouth/themes/cyanide/cyanide.script
```

Rename your directory **and** `.plymouth` file to the field in `[Plymouth Theme].Name`.
In our case that's `cyanide` --> `cyanide/cyanide.plymouth`.

*This is necessary as `plymouth-set-default-theme` currently looks for `${THEME_NAME}/${THEME_NAME}.plymouth`*

With that done, make sure `.plymouth` and `.script` are both executable 
(`chmod +x cyanide/cyanide.{plymouth,script}`).

You can then move the whole `cyanide` directory to the plymouth data directory.

## `plymouth --show-splash` crashes immediately

Probably the bootscreen's scripts aren't executable.
The `.script` and `.plymouth` files can be found in the install location and have to be executable.

If there are other issues, run `plymouth --debug --no-daemon` in a separate shell (don't forget to `export DISPLAY=:2`)
 and then rerun the simulation line from above.
`plymouthd` should spit out some (hopefully readable) debug lines.

## The logo says "bizcom"

Some boot screens expect a logo to be present.
For some reason, that location is [hardcoded][plymouth github] to be `/usr/share/plymouth/bizcom.png` on opensuse.


# Conclusion

There's **a lot** of room for improvement, but I'm glad this exists at all **and** that it's opensource.
Figuring this out would've been a terrible pain in the neck otherwise.

--------------

Image by [drunken_duck on openclipart.org](https://openclipart.org/detail/26401/-by--26401)

[Cyanide Plymouth Theme]: https://store.kde.org/p/1487814/

[KDE store]: https://store.kde.org/browse/cat/108/order/latest
[plymouth]: https://www.freedesktop.org/wiki/Software/Plymouth/
[plymouth github]: https://github.com/freedesktop/plymouth/commit/398eda4203ce1e47ba5c973db3a6418f43b4eea1
