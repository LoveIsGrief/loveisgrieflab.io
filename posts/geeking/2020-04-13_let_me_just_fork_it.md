Surely a few of us have found a project missing something we wanted it to have and considered forking it.
<!-- TEASER_END -->
For some reason or another that idea just fell flat because we either didn't want to "compete" with the official project
 , didn't want to go through the effort of maintaining yet another project, or myriad other reasons.

In my case, the things I'd like are mostly small and unimportant (or not worth the hassle) to the official maintainers.
Other times the project has intermittent updates that I'll miss downstream.
And sometime I don't want to stir up confusion by providing another package with a minor change that doesn't get updated.

Well, getting updates from upstream is now easier thanks to [`fork2gitlab`][fork2gitlab] ([repo][f2g-repo]).

# Fork to gitlab

## Why gitlab

Gitlab provides push and pull mirroring, which means updates upstream can be pulled into the fork,
 and updates on the fork can be pushed somewhere else.
What gitlab doesn't do however is handle merge conflicts.
So, if you forked a project and started making changes to the master branch, pulls from upstream are either overwritten
 or just fail.
However, that's the only problem and luckily it can be solved fairly easily.

## The solution

Handle merging ourselves in order to detect errors and handle them manually.
The manual treatment is in the form of a merge request which means human intervention.

Once the merge request is created a wrapper script can send a notification via twitter, email, Signal, carrier pigeon,
 or whatever medium deemed necessary.

## The flow
Forking with `fork2gitlab` / `f2g` has a very simple flow:

1. Fork a project with the script.
It will setup pull mirroring.

2. Manually create a branch and make the changes you like.

3. Let gitlab mirror upstream into the fork

4. Use `f2g sync` to attempt merging the source branch (most likely `master`) into the fork branch.
 This can be done as a cron job (that's how I do it), manually or however you want.
 
# Last words

It was beyond my laziness to find the gitlab issue tracker, create an issue, 
 wait for it to be implemented at someone's leisure, or for me to relearn Ruby and implement the feature.
Ah... another reason I create forks...


[f2g-repo]: https://gitlab.com/NamingThingsIsHard/collaboration/fork2gitlab
[fork2gitlab]: https://pypi.org/project/fork2gitlab/
