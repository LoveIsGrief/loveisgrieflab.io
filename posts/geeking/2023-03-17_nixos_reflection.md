I just gave it a shot and now it's my main OS, but only because it's stable.
<!-- TEASER_END -->

OpenSuse killed itself while upgrading between major versions 
 and I had managed to install NixOS with fulldisk encryption a month earlier or so. 
Had OpenSuse not shit the bed, NixOS would still be on a separate partition and then deleted to make space.

In the months since, I have actually contributed a package or two to nixpkgs.
Most of my OpenSuse setup has been migrated to NixOS and even my homeserver is running the OS too.

# The good

It's quite stable and upgrading is a breeze (once you find out how).

Once you figure out packaging a little, contributing to nixpkgs is also nice.
It's the first OS I could actually contribute to and get something merged into - all others really are too complicated
 to figure out and unfortunately the process is often lengthy.

For example, I wouldn't know where to start with packaging a `.deb`.
There are a bunch of tools, tutorials are outdated and mostly about packaging some C++ application 🤮
In that regard, NixOS is a pull request away on GitHub.

# The bad

## Documentation

The documentation might forever be the absolute worst part about `nix` and NixOS.
It just takes way too long to find out how to do stuff.
This makes the learning curve still pretty steep.

For example, I still don't feel comfortable enough to use [HomeManager]: a declarative method of managing your `/home/user`.
The homepage says one has to be comfortable with the `nix` language and the ecosystem - enough to debug problems
 and eventual data loss.  
Uh... no thank you.

## [flakes]

Also, a not insignificant part of the community is crazy about "flakes", which a supposedly some standardized way
 to write `nix` expressions and package or control stuff.
Unfortunately, flakes haven't been standardized themselves, weren't included in the main manual the last time I checked,
 are still in beta or alpha, and "subject to breaking changes".
I don't know about the rest of the NixOS community, but I'm not willing to spend a bunch of time learning about this
 only to rewrite it all (or big parts of it) or toss it after a breaking change.

## Stewardship

The Nix community stewardship / organisation feels like a big mess.
Probably this is due to the lack of easily discoverable and understandable documentation.
Without asking the community who's in charge of what or how things work,
 it's difficult to determine ownership, procedures, get a good overview of the steering groups within the community
 and how one might possibly join.

# The ugly

There's a good part of the Nix community that prides itself in being the "new" OS that's hard to understand.
Gatekeepers are everywhere and discussions about documentation are often waved away.
It reminds me of the early "RTFM" (Read The Fucking Manual) screamers, 
 which are even worse than the Let Me Google That For You (LMGFY) crowd.


----------------

All in all though, for now, it suits my needs: it's stable, I have the packages I want, and I kinda understand
 what I'm doing now for my purposes.

Should a functional and user-friendly graphical nixos manager or something ever come to exist, I might even help
 people switch to NixOS, but for now, there's no way I'm installing this on somebody else's computer and condemning
 them to late nights figuring out just how to do stuff.

[flakes]: https://nixos.wiki/wiki/Flakes
[HomeManager]: https://nixos.wiki/wiki/Home_Manager
