After multiple attempts, this distro makes me madder and more frustrated than any other OS.
Dependence is the name of the game
<!-- TEASER_END -->

I've been using linux for nearly a decade now.
It didn't start out easy but things quickly made sense and luckily desktop environments hid a lot of complexity.
There have been moments of unadulterated rage while using it, and though not as bad as with MacOS or Windows,
 NixOS has been an entirely different beast altogether.

MacOS restricts you at every turn with popups here, forced drag and drop there, lacking customization options
 in other places, and just an Apple way of "we know better what's for you, you luser".
Microsoft has been around long enough for workarounds to have been found for a lot of its questionable decisions,
 the ecosystem is ripe with freeware and lots of tools to bend windows towards your will.
Linux distros benefit heavily from the opensource approach where if you're a developer and 
 the error message isn't explicit enough, the `man` page or `--help` isn't helpful, 
 a forum or live chat doesn't have the answer, one can always look at the code to try and figure out things alone.

# The bane of documentation

A project without documentation is not worthless, but damn near so.
Your audience probably won't be able to use it without asking people in the know 
 or investing a lot of time exploring the ins and outs of it.

Poorly written documentation can actually make the experience bad too.
If it isn't kept up to date, doesn't explain what the tool is, what it does, why it does it, and doesn't do so for
 beginner, experienced users and experts, it can alienate some or most of its (to be) users.

Good documentation, well, doesn't do what poor documentation does.
Wonderful examples thereof are [django], [python] and [ubuntu].

NixOS however decides to provide a documentation that's either too deep, too specific, or too broad.
Even with a decade of experience with Debian, Ubuntu and OpenSuse, I couldn't make it useful for two usecases:

# My usecases

Operating systems and distros are just tools for a job.
Everybody has a different job and not all tools fit every job.

## Easy deployment of servers with declarative configuration

After experiencing deployments with [Ansible] and [SaltStack] (daemonless and daemonful deployment),
 the promise of `nix` to allow simply declaring the OS state once without having to rewrite it
 when deploying or provisioning was very enticing.
Just the built-in tools are supposedly required on NixOS - no need for some other provisioner.

To me, it held the equivalent promise of NodeJS: one language for backend and frontend.
NixOS was supposed to be "one set of tools for local and remote".

Then problems started rolling in:

**Despite using `nix`, installing software works differently when using NixOS and non-NixOS systems**

See the [wiki for examples][cheatsheet]
This is one of the biggest WTFs thus far.
If I test `nix` in an environment that isn't NixOS, the commands and behavior is totally different.

**Finding out how to configure services isn't straight forward**

Some services happen to have documentation on the [wiki] or in the [manual][nixos manual],
 but many are (at least to me) just black holes and yield no results on search engines.
It's completely unclear to me how to find out how to configure a service and which options are available.
This is also due to the fact that manual changes to configuration outside of the package manager
 will be overwritten by `nix`.

I forget the other problems since I didn't keep a log, but these were definitely showstoppers. 

## Separate development environments with different dependencies

I primarily work with Python atm (learning rust) and while I like to use newer versions, many projects still use 3.6.
That can lead to the usage of newer, unsupported syntax but having multiple python versions installed 
 can lead to confusion for system programs - some of which still rely on 2.7(!).

Naturally, `nix-shell` (a tool to create environments with only specific packages in them) makes this possible.

In order to test it and just see how things work without possibly polluting my main system, I went for docker.
It's not `NixOS` but Alpine running `nix`, which should be OK, right? ... right?

Wrong.

**`--help` uses `man`**

```shell
$ docker run --rm -it nixos/nix nix-env --help
error: command 'man nix-env' failed: No such file or directory
```
There's no documentation installed in the docker image, which isn't a problem.
However, in order to get the documentation one has to, according to the live chat, "rebuild" - whatever that means.

Great. **Another** thing I have search. Just to get a friggin' help message.

**Finding and installing packages is annoying**

Python projects use their own dependency management with `pip`, sometimes combines with [`poetry`][python-poetry].
How do you find those on Nix though?

Either through the [website][packages] (which I somehow discovered after the commandline) or with `nix-env`

```shell
$ nix-env -qaP pip
error: selector 'pip' matches no derivations
# nope
$ nix-env -qaP 'pip.*'
nixpkgs.aespipe                                  aespipe-2.4f
nixpkgs.epiphany                                 epiphany-40.2
aespipe-2.4f
epiphany-40.2
gitlab-ci-pipelines-exporter-0.4.9
lesspipe-1.85
libpipeline-1.5.3
ocaml-pipebang-113.00.00
ocaml4.12.0-ppx_pipebang-0.14.0
ocaml4.12.0-tcpip-6.1.0
nixpkgs.python38Packages.pipBuildHook    pip-build-hook.sh
nixpkgs.python39Packages.pipBuildHook    pip-build-hook.sh
nixpkgs.python38Packages.pipInstallHook  pip-install-hook
nixpkgs.python39Packages.pipInstallHook  pip-install-hook
nixpkgs.pipelight                        pipelight-0.2.8.2
nixpkgs.postgresql11Packages.pipelinedb  pipelinedb-1.0.0-13
nixpkgs.pipenv                           pipenv-2020.11.15
nixpkgs.piper                            piper-0.5.1
nixpkgs.pipes                            pipes-1.3.0
nixpkgs.pipewire_0_2                     pipewire-0.2.7
nixpkgs.pipewire                         pipewire-0.3.30
nixpkgs.pipework                         pipework-2017-08-22
nixpkgs.pipr                             pipr-0.0.15
nixpkgs.pipreqs                          pipreqs-0.4.10
# nope
$ nix-env -qaP '.*pip.*' | grep -i python
nixpkgs.python39Packages.pipBuildHook            pip-build-hook.sh
nixpkgs.python38Packages.pipBuildHook            pip-build-hook.sh
nixpkgs.python38Packages.pipInstallHook          pip-install-hook
nixpkgs.python39Packages.pipInstallHook          pip-install-hook
nixpkgs.python38Packages.apipkg                  python3.8-apipkg-1.5
nixpkgs.python38Packages.bootstrapped-pip        python3.8-bootstrapped-pip-21.0.1
nixpkgs.python38Packages.pip                     python3.8-pip-21.0.1
nixpkgs.python38Packages.pip-tools               python3.8-pip-tools-6.1.0
nixpkgs.python38Packages.pipdate                 python3.8-pipdate-0.5.2
nixpkgs.python38Packages.pipx                    python3.8-pipx-0.16.3
nixpkgs.python38Packages.python-pipedrive        python3.8-python-pipedrive-0.4.0
nixpkgs.python38Packages.r2pipe                  python3.8-r2pipe-1.5.3
nixpkgs.python38Packages.torchgpipe              python3.8-torchgpipe-0.0.7
nixpkgs.python39Packages.apipkg                  python3.9-apipkg-1.5
nixpkgs.python39Packages.bootstrapped-pip        python3.9-bootstrapped-pip-21.0.1
nixpkgs.python39Packages.pip                     python3.9-pip-21.0.1
nixpkgs.python39Packages.pip-tools               python3.9-pip-tools-6.1.0
nixpkgs.python39Packages.pipdate                 python3.9-pipdate-0.5.2
nixpkgs.python39Packages.pipx                    python3.9-pipx-0.16.3
nixpkgs.python39Packages.python-pipedrive        python3.9-python-pipedrive-0.4.0
nixpkgs.python39Packages.r2pipe                  python3.9-r2pipe-1.5.3
nixpkgs.python39Packages.torchgpipe              python3.9-torchgpipe-0.0.7
```

Oh great!
It's `nixpkgs.python38Packages.pip`.
Let's install it

```shell
$ nix-env -i nixpkgs.python38Packages.pip    
error: selector 'nixpkgs.python38Packages.pip' matches no derivations
$ nix-env -i python38Packages.pip    
error: selector 'python38Packages.pip' matches no derivations    
```

Ah... well. Now what?

Unfortunately the [online manual][nixpkgs.python] doesn't explain or show what is happening here 
 so something as simple as installing pip doesn't work.

# Looking for help

Since the manuals don't provide the simple information I need to do simple things without trying to craft
 the perfect search query that will put me on the right path, I was forced to use live chats (IRC/Matrix).

Though it's nice that people responded quickly with a "just do X" and that "X" worked, it doesn't help understand
 what's going on and why "X" had to be done instead of "Y".
Trying to explain the usecase and get more information or even a link to something that explains what's going on
 was met with more "just do X" and (I paraphrase)

> Many things aren't documented so just ask here

That immediately made me think of

> Give a man a fish, and he'll be fed for a day. Teach a man to fish, and he'll be fed his whole life.

After describing the environment I was working in (docker), the immediate response was

> Just install `nix`. You can uninstall it by removing `/nix` when you're done

Now, I've done that before with [`guix`][guix], which turned out to be another grueling experience that polluted
 my entire environment and wasted a lot of time getting rid of.
Since `nix` must somehow be found after installing, that implies changes in at least `PATH`, 
 and if `guix` is any indication, definitely changes to other parts of the system or environment.

And finally, what the hell is it with "just `curl | sudo bash -`" installs?
Why do people think this is secure? It's the bloody equivalent of downloading and installing an unknown program
 on windows **and** giving it admin access. 🙉🙉🙉🙉

I've come across this attitude before and there's not much one can do online about it.
One can only hope that NixOS forums aren't like the live chat.

# Conclusion

NixOS has a great selling point and great promise.
It is however very unfortunate that the documentation is far from complete and very unfriendly to beginners like me -
 who want to do things quickly while building up an understanding.
A great amount of work and time has gone into it, but the top->down approach of documentation is heavily lacking.

No OS has made me as frustrated as NixOS.
Every step of the way has been filled with roadblocks and the unwillingness of the community to explain things
 just doesn't mesh with me at all.

Had I time, I'd stream my experience with NixOS, which would probably include me screaming expletives in multiple
 languages at the screen, but alas I have none of that.
If anybody reads this (probably only me when I look back at this): if you want to use NixOS of `nix`, 
 you better be ready to dive in the deep end to use this time sink.

I'll look at the NixOS documentation in 5-10 years again before I touch that distro again.
Hopefully it'll be **much** more beginner-friendly without requiring a trip to community support for every, single
 command.

For now, fuck NixOS.

[Ansible]: https://www.ansible.com/
[cheatsheet]: https://nixos.wiki/index.php?title=Cheatsheet&useskin=vector
[django]: https://docs.djangoproject.com
[guix]: https://guix.gnu.org/en/
[homepage]: https://nixos.org
[nixos manual]: https://nixos.org/manual/nixos/stable/
[nixpkgs.python]: https://nixos.org/manual/nixpkgs/stable/#python
[packages]: https://search.nixos.org/packages
[python]: https://docs.python.org
[python-poetry]: https://python-poetry.org/
[SaltStack]: http://saltstack.com
[ubuntu]: https://help.ubuntu.com/
[wiki]: https://nixos.wiki