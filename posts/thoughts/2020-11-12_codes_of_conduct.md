Projects with contributors from all walks of life need some rules,
 but some projects go a little far.

<!-- TEASER_END -->

Back when the issue blew up due to [Linux adopting a code of conduct][news: Linux CoC] written by what some people
 perceived as a radical feminist, I kinda got it.
The project head, Linus Torvald, was known for his inflammatory rants and expletive-filled code reviews.
It probably deterred a few people from joing a community that condoned such behavior,
 because even though Linus was right, he wasn't expressing his displeasure diplomatically.

# Behavior online and in the opensource community

To his credit, Linus recognized the error in his ways and has done well to change -
 at least from the limited amount of messages I've seen.
He is however an exception.
The internet is filled with people looking for a fight or troll, unable to communicate diplomatically,
 unable to accept constructive criticism or to give it, and unwilling to change.
It's not that much of an issue when they stay amongst the own foul-mouthed kin, but when they spead
 and mix (which is natural), that's where the problem lies.

Most opensource maintainers have experienced rude and entitled users demanding a feature or a fix.
There are of course also developers who enjoy brigading projects to try and insert their opinion
 either for fun or to attempt an enforcement of their views upon the rest.
Examples include:

 - Why don't you reimplement this in Rust?
 - How can such a project not have feature X?
 - The developer is ignoring us! It's clear that...
 - Look at the code of this project! Which idiot wrote this?

We all have seen similar or worse examples, but most of us just scroll past it and say nothing.
Even worse, some use participate!
How often do you see a user on a platform like Gitlab or Github tell a peer to kindly cool it
or be reported?
When was the last time **you** did it?

That raises the question: to whom should I report it and in which cases?
Sure, you can report it directly to the platform, but they are mostly slow and don't really care.
Some platforms haven't responded to my reports months later.
People on other platforms have had [similar experiences][twitter report example]
This is where a Code of Conduct comes into play.

# The good and the bad

In my opinion, a good code of conduct should really just be about that: how to conduct yourself.
It should include what's permitted, what isn't, what can happen if you don't and who to talk to.

Most of them can be summed up to:

 - be nice
 - assume good faith
 - if you aren't nice, you're out

It's kindergarden level, yet somehow, humans keep not abiding by it 🤷.

## The good

**[KDE]**

> This Code of Conduct presents a summary of the shared values and “common sense” thinking in our community.
> The basic social ingredients that hold our project together include:

>  * Be considerate
>  * Be respectful
>  * Be collaborative
>  * Be pragmatic
>  * Support others in the community
>  * Get support from others in the community

Personally, I find this to be one of the best written Codes of Conduct out there.
[Their version][KDE CoC] doesn't include a contact point, but besides that it doesn't:

 - push a political agenda
 - declare a belief system
 - philosophise about roles in society or the universe

**[Debian]**

This CoC doesn't have a summary like KDE, so here are just the headers.

1. Be respectful
2. Assume good faith
3. Be collaborative
4. Try to be concise
5. Be open
6. In case of problems

Right off the bat, you can see they followed their own rule: Be concise. I ❤️ it.

Once again, no political or social agenda, no belief system, no grand declaration of self-worth, etc.
Especially rules #1, #2 and #6 are good. 
The rest are part of #1 but I guess they had to be expanded upon.

 

## The bad

I'll intentionally not point out the ones I consider bad (the list is large).
Knowing the internet, if by some bad luck extremists of either side read this, they'll blow it
 way out of proportion.

I'm not afraid of voicing my opinion, so they won't limit my free speech, but there's no desire
 to deal with extremists.
There's better ways to spend one's time.

With that said, there are Codes of Conduct that go overboard 
 by adding philosophies, world-views, opinions, political statements, etc.
Some even go as far as punish you for actions outside of the project that they don't agree with.
Even others ban certain words they deem offensive e.g master/slave, blacklist/whitelist, ... 

What I have noticed is large companies adopting CoCs for marketing purposes.
People celebrating this just baffle me a little.
Isn't it obvious that companies only care about image?
Once the trend dies out, new and old companies will simply drop it.

# Closing words

CoCs are a good idea and unfortunately necessary, because people just can't stop being bad.

Some however go beyond the essence of a CoC are a good indicator for me not to join 
 the project or group promoting them.
I want to talk about the project, solve interesting technical problems, learn new technologies,
 and write nice, functioning code, maybe even make friends, not walk on eggshells.
Nice and functioning code might not always be the case as I am an employee at the bug factory,
 but that's better than being caught up in people's social issues, political agendas and whatnot.
If I wanted all that, I'd join a political party or some kind of movement.

--------------------------

Thumbnail image thanks to [narrowhouse](https://openclipart.org/artist/narrowhouse)

[KDE]: https://kde.org
[KDE CoC]: https://kde.org/code-of-conduct/

[Debian]: https://www.debian.org/
[Debian CoC]: https://www.debian.org/code_of_conduct.en.html

[news: Linux CoC]: https://itsfoss.com/linux-code-of-conduct/

[twitter report example]: https://debianpeertube.com
