Free public WiFi could maybe be less of a problem if participants were anonymous by default.
Not only from another, but also on the internet.
<!-- TEASER_END -->

Mesh networks have had problems in some countries due to the risk of being sued for the behavior
of guests on their networks doing stupid and/or illegal things.
It's possible to limit the access-points to certain domains, reject all unencrypted traffic, etc.
 but all of those depend on some kind of packet inspection.
Personally, I don't like packet inspection as it is an intrusion in the person's traffic and
 therefore not network neutral.

So, what am I proposing exactly?

Rerouting all traffic through [TOR] or [I2P].
Users would thus be anonymous by default and if it's their only network connection is over the
 access point, that means all network traffic would pass through the overlay network.

Not only would it be a method of anonymizing users, they could also be introduced to I2P
 or other overlay networks like [Yggdrassil] (which I personally haven't ever used).

# How ?

Well, I plan to try this with a raspberry Pi, if it doesn't kill my SD-card again and if the WiFi
stays stable for a while.
Using `iptables` it should be possible to force all traffic through a transparent SOCKS proxy.
This should probably exist already, or a least look similar to the [I2P docker proxy] I'm working on.

---------------

Image contributors: [cyberscooty] & [Flugaal]


[cyberscooty]: https://openclipart.org/detail/195841/logo-free-wifi-inside-for-a-cafe
[Flugaal]: https://commons.wikimedia.org/wiki/File:Tor-logo-2011-flat.svg

[I2P]: https://geti2p.net/
[I2P docker proxy]: https://gitlab.com/NamingThingsIsHard/privacy/i2p-docker-proxy
[TOR]: https://www.torproject.org/
[Yggdrassil]: https://yggdrasil-network.github.io/
