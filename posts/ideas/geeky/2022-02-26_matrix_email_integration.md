Email is old, it's insecure and it has to go. But, it's not yet fossilized and we can't get rid of it in one go.
Maybe Matrix can help with the transition...
<!-- TEASER_END -->

We all know about email, but not everybody knows about [Matrix] - the protocol not the movie.
If you haven't heard of it before, it's basically a protocol initially designed for chatting and bridging different
 chat protocols like IRC, XMPP, ICQ, MSN Messenger, Skype, etc.
There have been many chat clients allowing you to chat using those disparate protocols, but none (to my knowledge)
 that have **bridged** them. The chat applications just let users of protocol A chat with other users of protocol A.

"What is **[bridging]**?" you may ask.
Well, it allows chat rooms to have users from multiple different protocols.
That means for example that in a chat room IRC users can talk to WhatsApp users, Signal users, Skype users, etc.
Of course there are things to consider e.g Signal is encrypted and IRC is not --> what happens? 
That however is not the focus of this post.

# The idea

What if we could just use Matrix to send emails to people without ever seeing an email client or explicitly
 signing up for an email service?
Imagine this:
You head over to https://element.io (the official chat application of matrix), create a matrix account,
 and then send an email - directly from within Element Web - to your best friend bestfriend@tutanota.de.

It's not possible yet, but could it be?

# Possible solutions

Both solutions will require the first step: email account  creation at signup to the matrix server.

[Synapse], the reference Matrix server implementation in Python, supports plugins/modules.
These allow registering callbacks to, among others, user signups.
There, it could be possible to at least create an email account on the email server or connect the account
 to an existing email account on another host using IMAP and SMTP.
The possibility to do this later should also be provided, but how is a question to be answered at another time.

## Subspaces and discussion rooms

_Spaces are just room groups to build a hierarchy of rooms._
_The spaces and rooms themselves can be made private, so for example an organisation can have their own space._

In this solution, after the user signs up and an email account is connected, a private "Email" space is created.
Within that space a few  subspaces that look like those in an email client are created

 - Inbox
 - Draft
 - Deleted
 - Archived
 - ...

The room within the subspace will then correspond to a discussion e.g

 - Inbox
     * `2022_02_20 - A test message`
     * `2022_01_01 - Happy New Year!`
     * `2021_12_25 - Christmas wishes!`

Responding to an email would then be opening the discussion and typing the response, just like in a normal chat.
Sending a new email could be creating a room in the `Draft` subspace, adding users to the room by email address,
 and just sending off the message: like in a normal chat.

Some matrix clients also allow being extended with `<iframe>` embeds that communicate with an API.
This would allow communicating with a bot or the Synapse module to do email client things e.g filtering the inbox,
 archiving emails, spam filtering, etc.


## Discussions are threads

In this mode, starting an email discussion with a person would just be starting a new chat with that person's email.
Emails would then be the individual messages and responses would then create threads e.g

- Private chats
  - Justin Pool <justinpool@somewhere.test>
    - Me: Message 1  <-- Thread
        * J: Response to Message 1
        * Me: Response to Response
    - J: New message <-- Thread
        * Me: Response to new message

Emails with multiple people would just be rooms with those people.

# Pitfalls

The major unknown for me is how Matrix works with accounts on other chat networks.
Can "virtual" users be created to represent remote users and how can one interact with them?

## Changing recipients

Add or moving recipients to a conversion is done in email with carbon copy (CC).
Sharing a conversation or a message with a recipient without that recipient knowing who else is in the conversation
 is done by blind carbon copy (BCC).
If BCC is used right at the beginning, it's a nice way of blasting multiple recipients with the same message
 and those recipients not knowing who else received the message.
However, BCC used later in a conversation can still reveal its participants if the entire conversation is included.

CC could be implemented by adding people to rooms. BCC, though, I can't think of a practical way to implement.

## Spam

Well... yeah.

------------

So, that's it.
Probably a lot more thought has to go into this, but it might be an interesting challenge.

Since Matrix is all about bridging stuff, it would also make it possible to respond to emails over WhatsApp, Signal,
 Telegram, or whatever other protocol you're using.
That also requires more thought and I need to test those bridges to see what it looks like.

---------------

[Element]: https://element.io
[bridging]: https://matrix.org/bridges/
[email bridges]: https://matrix.org/bridges/#email
[Matrix]: https://matrix.org
[Signal]:  https://signal.org
[Synapse]: https://matrix.org/docs/projects/server/synapse
