For a long time I've felt that although copyright may have good motives, it has been subverted and transformed into
 something very ugly.
<!-- TEASER_END -->
No longer is it a tool to protect creators from plagiarism, allowing them to live from their creations and thus
 foster more creativity.
No. It's a tool secure the already wealthy who buy up lots of copyrighted works or force creators to sign away 
 their rights.
Sure, they also create things, but make access to those things difficult in order
 to artificially create scarcity and raise prices (see Forced Artificial Scarcity aka [FARTS]).
To top all this off, remixing, reforming, adapting and improving things is only legal by paying the copyright holder - 
 regardless of monetary gain.

Luckily, there might be a solution and it's called [Creative Commons][CC].
I'm no lawyer, but from what I understand, it's a liberal copyright license with more possibilities
 than the current, restrictive copyright license.
They all start with CC-BY and have another ending tacked on. Here's what they mean
 
 -  BY – Credit must be given to the creator
 -  SA – Adaptations must be shared under the same terms
 -  NC – Only noncommercial uses of the work are permitted
 -  ND – No derivatives or adaptations of the work are permitted
 
They all permit remixing and reusing for non-commercial use - something copyright is very difficult about.

# What is needed?

Attribution and non-commercial use is nice and all, but we aren't hippies that can live from
 recognition and "thank you"s. What can they live from?

**Donations**

Patreon is making a nice buck as an intermediary for donations and there are quite a few people
 making a living using the service.
Lots of open-source projects are managing to survive on donations alone and NGOs like Mozilla
 even supposedly make 500 **million** $ a year.
To be fair, whether the entirety of that money is donated is doubtful.

In any case, it's working.

But before even getting to donations, where can you expose your creations?
There are already quite a few platforms for that, but they have a problem

**Advertisements**

Cotdamn, bloody ads are all over the friggin' place.
We have all heard of non-intrusive ads, but where can you find these mythical things?
Non-intrusive ads might have existed at one point, but now...

We need platforms that don't blast you with a dumb video that eats up your data all in the name
 of trying to sell you a product you don't need anyway.
Creators are free to choose whether they will take that kind of money and
 embed ads into their content in addition to donatios, but the default shouldn't be "money by ads".
 
Alright, now we've talked about new content, but what about "old" content aka copyrighted content?
There's lots of it out there and people are always seeking access to it.
When access is difficult, the price doesn't even matter.
You could sell your content at 1¢ but if it's behind a subscription, DRM, geo-blocking and ads (Youtube Red anybody?),
 I betcha most people are going to go "Why hello there sharing platform!".

So how do we get to a state where CC is the default?
Where it doesn't make sense to copyright it and put a whole infrastructure and industry
 to protecting protect access to this content for fear of plagiarism and lost revenue? 

# CC-BY-FORCE

If copyright holders cannot find the person(s) they want to persecute, the effort involved in
 demasking them for persecution, and a large majority moves to a donation based model,
 they will have no choice but to either follow or die.
 
**How?**

Anonymous, distributed or federated distribution platforms.

The only component missing is the **anonymous** part.
And I'm not talking about sharing platforms, but really distribution.

[Peertube] and [Funkwhale] for example are great alternatives to Youtube and Spotify.
They are open-source, federated, give admins and users a lot of freedoms,
 and allow creators to link to donation options.
Most of the content provided there is CC, but when copyrighted content is hosted,
 the admin gets a notice.
Should they fail to take down the offending content,
 they can be liable to charges.
That disappears when anonymity is provided.

## A possible timeline

Once these federated services hit the darknet, this is what a timeline would look like.

**Arrrrr**

There will be little to no content creation but liberal amounts of
 "piracy" on the high seas of the tenebrous net.
The age of porn thumbnails leading to gore and other disgusting stuff will return.
Not to say that any of this doesn't exist on the clearweb, but those services are always
 in danger being somewhat short-lived.
A few months or years until either someone judges them too costly to ignore, or leeching without
 giving back to the site owner and ads have failed, will force a shutdown.
 
Once an anonymous fediverse (possibly atop a distributed storage like [IPFS] or [TahoeLAFS])
 has been established, I doubt there'll be a way to put that genie back into the box. 

**Age of discovery**

Copyrighted content won't die straight away, but once accessing such content becomes
 free, fast and secure with no possibility to donate (besides merchandise of course)
 it wouldn't surprise me to see profits drop.
Not only that, but there should be a bigger incentive to improve the federated services.
Youtube, HBO, Disney, Netflix, Spotify and other DRM nests might have to innovate a lot to keep pace.

Innovation will be the name of the game.
It might become necessary to do whatever is possible to stop the bleeding of customers to the anoniverse.
But how do you compete with free, easy and well... mediocre speeds?
You can't wait for the speeds to get fast enough to compete with your product, right?

Glossier content will surely become a selling point that will be hard to compete with on the anoniverse.
I mean... a 4k/UHD film can be about 4GB with HVEC 265 at 24fps and 13Mb/s. 
That means minimum 1.65 MB/s.
HD clocks in at about 6Mb/s --> 0.75MB/s.

Anonymous networks like TOR are fast (1M/s easy) but not optimized for P2P.
I2P is optimized for P2P but slow - you'd be lucky to get 0.2MB/s.

But once speeds catch up, where can they go besides requesting customers to drop 1k on a 4K 15" screen
 where you can't even see the difference between HD, WQHD and UHD?
 
Once copyright profits have severely diminished and anything and everything digital has been shared,
 creators won't just disappear and call it quits.
Services like [Bandcamp] and [Jamendo] aren't doing too badly.
Creators on [Patreon] are thriving and [CC] is seeing more and content using the permissive license. 

This is where we'll end up:

**Donation based economy**

I believe it's the most logical conclusion.
People who really like a creator and their content will donate to see more of it.

It won't be necessary anymore to attempt to DRM and geoblock content
 because it'll end up on the anoniverse anyway.
There won't be a need to stop making content simply because one country isn't consuming enough -
 the rest of the world might consume way more.

It may be come quite cut-throat since consumers are fickle, but it may also lead to a lot of diversity.
Content from all over the world will be available freely, immediately, and securely.

One problem however will be verification.
On the darknet, where ownership means little, donors will want to be able to know that their money
 ends up in the right hands.
I'm sure someone will come up with some kind of a solution.

---------------

Anyway... I've written enough, but I think this is but one possible future.


[Bandcamp]: https://bandcamp.com/
[CC]: https://creativecommons.org/
[FARTS]: https://www.cracked.com/article_18817_5-reasons-future-will-be-ruled-by-b.s..html
[Funkwhale]: https://funkwhale.audio/
[IPFS]: https://ipfs.io/
[Jamendo]: https://www.jamendo.com/
[Peertube]: https://joinpeertube.org/
[TahoeLAFS]: https://tahoe-lafs.org/trac/tahoe-lafs
