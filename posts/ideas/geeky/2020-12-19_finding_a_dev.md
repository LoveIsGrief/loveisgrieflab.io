Many devs work on things in solitude, even those that share those projects online.
Even more have ideas, but how could we interest people to collaborate?
<!-- TEASER_END -->

I have an idea - probably a bad one - but an idea nonetheless: a streaming talk-show about people's ideas.
Viewers would then be able to discover interesting projects they might be willing to participate in.
With a good following, projects could also be promoted this way for use as well.

Why don't I do this myself? Well... I'm one of those people looking to collaborate 😅
It frankly isn't in the cards for me to spend much time on streaming.
I'd rather leave that to the willing.

In any case, what could this look like?

# Format

The most appealing format to me would be a host with about 5 guests.
Each guest would get 5-10 minutes to present their idea or project,
 followed by another 5-10 minutes of discussion with the host and guests.
That gives us about 50-100 minutes to listen watch people talk about their passion.

The goal wouldn't be to poke holes in a person's idea/project but to get a more 
 complete picture of what was presented.
Not only guests but viewers could ask questions per chat,
 and the host could select the most interesting ones.


# Getting guests on the show

This is always a difficult one, but the most obvious route would be providing an easy method of 
 contact, and a clear description of what people have to do to get on the show.
Hosts are of course free to do what they want, but I think the basic requirements to get accepted
 could be an existing project page with:
   
   * A README page or other description of the project
   * A contributing guide 
     _New users should be able to join the project without wondering how_ 
   * Tasks tagged by difficulty that interested people can pick up **or**
     a list of unanswered questions 
     _New users should know how they can help developing the project_
 
Hopefully with those, it would be clear that the guest has put a some thought into the project.

# Beyond the talkshow

Since it's not possible to always be present as a viewer, a search list of VODs would be very helpful.
The VODs would hopefully include a description with all the of the information necessary to find
 the guests' projects and presentation.

A public schedule might also be nice.
It would serve as confirmation for guests and of course also serve a little advertisement for viewers.

# Conclusion

A well-organized show with a corresponding (optional) website, promotion and selection mechanism
 could grow interest in development as a whole.
That could lead to greater participation and collaboration on projects or at least entertain
 developers in a thought-productive manner.

Hopefully, this would be an incentive for people to sign up to get on the talk-show.

--------------

Clipart by [Fractale] from [OpenClipart]

[Fractale]: https://openclipart.org/artist/Fractale
[OpenClipart]: https://openclipart.org/detail/241970/hexagon-internet-of-things
