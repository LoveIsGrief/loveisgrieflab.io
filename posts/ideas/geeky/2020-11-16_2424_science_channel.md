Let science be my noise and focus!
<!-- TEASER_END -->

This has popped up multiple times in the past when I would have nothing to do and just wanted to lean back to let
 my senses get showered with something to keep my mind busy.
Often times, I've just had enough of watching educational videos, reading articles about how bad the world is again,
 or even worse, scrolling aimlessly through my newsfeed.
What I would really like is to have something uplifting and entertaining at the same time, but that normally means
 watching some stream.

There are also times I would just like something to play in the background while I do other things.
Music is nice, but not all the time and streams of people doing something unimportant just... don't feel right.


What would hit the spot would be: a 24/24 stream (or group of streams) about science.

I can't be the only one... wishing for this.

# Really? Science? Every minute of the hour?

**Yep!**

There's so much going on on in the science world, but we just get to read or see a tiny bit of it.
We are bombarded with trivial, sensationalist and extremely biased news about... I don't even know anymore -
 I don't own a TV anymore or follow world news anymore.

# Soo... what would it look like?

A worldwide available stream on as many platforms as possible like Facebook, Twitch, Facebook, Periscope, Youtube,
 Peertube ([they merged it recently][Peertube live]!), Vlive ([Korean][Vlive]),...  anywhere.


## Programming

This is the major question: what should be shown on the stream?

**News**

Let's start with the most obvious.
Viewers should know about all the sciencing that's going on worldwide.
People should be informed about the interesting, promising, new, novel and certainly peer-reviewed research.
But not only research is important - there are companies and groups out there doing and building interesting things.
Science is impacting the world in many different ways and we should be made aware of that.

**Thematical panels**

What I've often missed is in-depth discussions about science that are comprehensible to those who haven't studied
 or done research in the particular field or disciple.
 
I can envision live, in-depth interviews with a panel on an interesting paper to make it understandable for the public.

For example, when gravitational waves were confirmed, most people didn't know what that was or what it meant.
I certainly didn't.
One could imagine there should've been a panel discussing the discovery.
A moderator with a layperson, an affiliated researcher or technician, and an unaffiliated researcher, maybe 2. 

**Documentaries**

I miss the old Discovery Channel and Galileo.
I miss when they were about teaching you something wonderful, breathtaking, marvelous and futuristic.
Last time I watched them they were about "The biggest machine to do X" or "How this thing is produced" or 
 "MEGA constructions: some building somewhere" or "MINI creatures: the wonderful world of bla".

Maybe my memory is failing me and I'm remembering different things, but I remember when they talked about space
 and what we needed to do to build things on Mars, get to Jupiter, or what pulsars and black holes are.
There were short videos on where certain sayings came from, the history of a tribe you'd never heard of that impacted us,
 why bacteria are good and bad for us, introductory videos to viruses, etc.

It felt more like education mixed with discovery than clickbait pop-science.
It made me see what the things I was learning at school had been, are being and could be applied to.
Watching those made me want to be a biologist, a chemist, a physicist, engineer, architect...
Those things made me wonder and marvel.

Maybe I'm not watching the right stuff, but fighting youtube's algorithms isn't fun. 
[TILVids][tilvids] is doing a good job of aggregating interesting stuff so maybe stuff from there could also end up on
 the stream.

### Proposal

**15 news / 45 in-depth**

Here we could get news on the hour for 15 minutes about the newest headlines.
Those could be papers released that day, announcements from conferences, progress reports on highly anticipated studies,
 recent technological developments, quick interviews with researchers, technical staff from places like ESA, ITER, LHC,
 and other places.

That could be followed by 45 minutes of an in-depth look at a certain topic.


**News news news**

Or, up to 10 minutes per news item to fill up the hour.
In this configuration the stream would only be news.
Whether that's possible would have to be seen, but that would mean non-news content would need another stream.

But where would we show all of this?

## Channels

Just like TV, there could be multiple channels:

**Main channel**

On our main channel we would have a mix of all sciences, where the major disciplines are discussed or reported upon.
It should be presented for laypeople (like me), who are curious about what's going on, but don't have a bachelors in
 all of the disciplines being streamed.

**Channels per discipline**

For laypeople with an affinity or those with more knowledge on a specific disciple, 
 surely a stream just about that discipline would be appropriate for them.


### Global scheduling
 
I think it would be great to have a **worldwide 6 hour rotation**.
Viewers could view content about science happening all around the world and each region would get the possibility
 to talk about what's happening in their region.
They could also mention what's happening in other countries, but I don't think that should be the focus.

<!--Generated with https://www.tablesgenerator.com/html_tables# --->
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
.tg .tg-txzb{border-color:inherit;color:#2E3436;text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-0pky">City</th>
    <th class="tg-0pky" colspan="6">Auckland</th>
    <th class="tg-0pky" colspan="6">Bangkok</th>
    <th class="tg-0pky" colspan="6">London</th>
    <th class="tg-0pky" colspan="6">New York</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">24 hours</td>
    <td class="tg-0pky">1</td>
    <td class="tg-0pky">2</td>
    <td class="tg-0pky">3</td>
    <td class="tg-0pky">4</td>
    <td class="tg-0pky">5</td>
    <td class="tg-0pky">6</td>
    <td class="tg-0pky">7</td>
    <td class="tg-0pky">8</td>
    <td class="tg-0pky">9</td>
    <td class="tg-0pky">10</td>
    <td class="tg-0pky">11</td>
    <td class="tg-0pky">12</td>
    <td class="tg-0pky">13</td>
    <td class="tg-0pky">14</td>
    <td class="tg-0pky">15</td>
    <td class="tg-0pky">16</td>
    <td class="tg-0pky">17</td>
    <td class="tg-0pky">18</td>
    <td class="tg-0pky">19</td>
    <td class="tg-0pky">20</td>
    <td class="tg-0pky">21</td>
    <td class="tg-0pky">22</td>
    <td class="tg-0pky">23</td>
    <td class="tg-0pky">24</td>
  </tr>
  <tr>
    <td class="tg-0pky">Local time</td>
    <td class="tg-0pky">15</td>
    <td class="tg-0pky">16</td>
    <td class="tg-0pky">17</td>
    <td class="tg-0pky">18</td>
    <td class="tg-0pky">19</td>
    <td class="tg-0pky">20</td>
    <td class="tg-0pky">15<br></td>
    <td class="tg-0pky">16</td>
    <td class="tg-0pky">17</td>
    <td class="tg-0pky">18</td>
    <td class="tg-0pky">19</td>
    <td class="tg-0pky">20</td>
    <td class="tg-txzb">15<br></td>
    <td class="tg-txzb">16</td>
    <td class="tg-txzb">17</td>
    <td class="tg-txzb">18</td>
    <td class="tg-txzb">19</td>
    <td class="tg-txzb">20</td>
    <td class="tg-txzb">15<br></td>
    <td class="tg-txzb">16</td>
    <td class="tg-txzb">17</td>
    <td class="tg-txzb">18</td>
    <td class="tg-txzb">19</td>
    <td class="tg-txzb">20</td>
  </tr>
</tbody>
</table>

In this model, the world would be divided in 4 time zones, where each gets 6 hours to broadcast. 
A city from anywhere within that 6 hour zone could be the broadcaster.

This opens open multiple possibilities:

**3 hours fresh content + 3 hours replay of the last slots's programming**

Say Auckland just ended its shift and handed over to Bangkok.
Bangkok could play 3 hours of fresh, new content and then replay 3 hours from Auckland.

Unfortunately, London wouldn't play Auckland content unless they explicitly decide to during their own 3 hour period.

**3 hours fresh content + 3 hours replay of the 3 last slots programming**

New York could play original content for the first 3 hours and then play 1 hour from London, another from Bangkok
 and finally another from Auckland.
With this system, one could view content from all around the world in 4 hours.

**6 hours mixed content of local and worldwide science**

In this system the current location would have free reign on what they want to show.


# Where to start? Possible timeline?

Honestly, I don't have the time to do this, so for now it's really just an idea.
It could just start as somebody grabbing articles from RSS feeds of science journals and dedicating a bit of time
 on stream to reading and discussing them with the audience.
With a bit of luck and a boost in popularity, maybe actual scientists or people working in the field
 could come on and give their input on the items being discussed.

Maybe more people would engage and sign up to take over certain time-slots of the stream to make increase the time
 the stream is online.
With the number of people growing and participating, it could grow to a 24/24 stream and expand from there.

--------------------

Those are about all the thoughts I currently have on the topic.
If this inspires somebody to start, that would be amazing. 
If not... too bad. 
If I ever have the time for this, I'll give it a shot myself.

[Peertube live]: https://github.com/Chocobozzz/PeerTube/pull/3250
[tilvids]: https://tilvids.com/
[Vlive]: https://www.vlive.tv
