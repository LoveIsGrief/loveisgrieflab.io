School and university aren't about learning anymore but a filtering system for employers.
What could a better system look like?
<!-- TEASER_END -->

Even though I'm curious and critical (sometimes to a fault),
 school and university haven't really brought me much joy.
They both seemed very limiting and actually counter-inducive to learning and curiosity.
Long have I dreamt up ways of improving the system and even though I haven't studied
 psychology, pedagogics, andragogy or similar topics (yet), there are basic ideas
 that have been accumulating.
Whether these are correct, I will see later (through study and discussion).

# Basics

I can't seem to keep things short, so the headers will try to convey the major points.

## Life is about learning

These are my main thoughts on my place in the universe, education and the education system.

I don't believe we are here with a grand purpose.
Life is inherently meaningless as we are simply machines - very intricate machines,
 but machines nonetheless.
As such, everyone has to ~~choose~~ discover what it is they want to do with their life.

My goal and the goal I think we as species should have is knowledge acquisition.
Exploration and understanding of the unknown.
Of course we should have fun doing so, but everything else is second-rate.

## Money has corrupted education

Knowledge can be acquired in many ways and it isn't always academic.
In fact, I'd say most of the knowledge we acquire is **not** through academic means. 
We could however be learning so much more academically, but the system is not built for learning: 
 the system is built for money.
Cold hard money.

Money makes the world go round.
While I understand that a world without a way to attribute value to services and good just cannot work,
 it has taken up too much of an integral part of our lives and infected too many systems.

Money has corrupted the education system.
The system's main goal isn't to teach anymore, it's to prepare for a role in society and that role
 is to generate money for the country.
Education is simply a side effect because the system needs capable workers to make valuable products.

**A note on copyright**

Fuck it. That is all.

Anyway, how do we turn this relationship of education for money around?

# A learning society

Naming things isn't my strong suit, but it basically means what it says: make a society or group
 where learning and education are the focal point and monetary value will be generated as a side product.

How do we do this though?
If you dedicate your life to learning, how will you make a living and who's going to pay for it?
And what if you simply need a break from learning and want to be "productive"?
Theory need practice, right?

The [Swedish higher education][Swedish higher education] is free for Europeans 
 and so is [German higher education][German education costs].
While those two countries come to mind, there are surely more countries like that
 (probably Switzerland and Austria, but I'd have to check).
That's a good start, but I think we should go further than that:

## Pay people to study

Immediately ways to abuse this will come to mind like studying philosophy, art or other
 courses with no discernable output.
Maybe even repeatedly failing your studies only to being a new one.

This is why I think the solution is Universal Basic Income ([UBI]).
That, together with wages for output from studies that help society and the human race as a whole should be introduced.
From a very biased viewpoint, those would be the hard sciences: biology, chemistry, maths and physics.
Maybe softer sciences like social science could be included as long as they adhere to the scientific method.

UBI however is a pretty big step.
In future posts on education, I'll read up on what other systems are doing, how close they are to my goals,
 and what could possibility changed in order to achieve those goals.
 
## What about the teachers / lecturers

Everybody learns and everybody teaches in some way, but not every is fit to teach professionally.
I believe the system in that regard should also change to first redefine what a good teacher is and then reward them
 with high salaries.
Good teachers should be revered, reputed and be role models.
The profession should become one of the dream jobs out there.

How to that, I don't know yet, but I hope to work on it someday.

Anyway, that's it for now.
Maybe I'll add pictures in the future to interrupt the rambling.

-----------------

Post image is by a combination of Red Silhouette - Electron.svg and Red Silhouette - Brain.svg both 
by [ben from OpenClipArt]

[ben from OpenClipArt]: https://openclipart.org/user-detail/ben
[German education costs]: https://www.study.eu/article/study-in-germany-for-free-what-you-need-to-know
[Swedish higher education]: https://sweden.se/society/higher-education-and-research/
[UBI]: https://en.wikipedia.org/wiki/Universal_basic_income
